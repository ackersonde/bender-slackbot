package commands

import (
	"testing"
)

func TestCalculateImageTokens(t *testing.T) {

	// Test case 1: 1024x1024 = 765 tokens
	imageWidth := 1024.0
	imageHeight := 1024.0
	expectedTokens := float64(765)
	if tokens := calculateImageTokens(imageWidth, imageHeight); tokens != expectedTokens {
		t.Errorf("Expected %f tokens, but got %f", expectedTokens, tokens)
	}

	// Test case 2: 2048 x 4096 = 1105 tokens
	imageWidth = 2048.0
	imageHeight = 4096.0
	expectedTokens = 1105
	if tokens := calculateImageTokens(imageWidth, imageHeight); tokens != expectedTokens {
		t.Errorf("Expected %f tokens, but got %f", expectedTokens, tokens)
	}

	// Test case 3: 3072 x 1728 = 1105 tokens
	imageWidth = 3072.0
	imageHeight = 1728.0
	expectedTokens = 1105
	if tokens := calculateImageTokens(imageWidth, imageHeight); tokens != expectedTokens {
		t.Errorf("Expected %f tokens, but got %f", expectedTokens, tokens)
	}

	// Test case 4: 1728 x 3072 = 1105 tokens
	imageWidth = 1728.0
	imageHeight = 3072.0
	expectedTokens = 1105
	if tokens := calculateImageTokens(imageWidth, imageHeight); tokens != expectedTokens {
		t.Errorf("Expected %f tokens, but got %f", expectedTokens, tokens)
	}

	// Test case 5: 1280 x 960 = 765 tokens
	imageWidth = 1280.0
	imageHeight = 960.0
	expectedTokens = 765
	if tokens := calculateImageTokens(imageWidth, imageHeight); tokens != expectedTokens {
		t.Errorf("Expected %f tokens, but got %f", expectedTokens, tokens)
	}

}
