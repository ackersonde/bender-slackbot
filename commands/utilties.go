package commands

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strings"

	"gitlab.com/ackersonde/bender-slackbot/structures"
	"golang.org/x/crypto/ssh"
)

func remoteConnectionConfiguration(config *structures.RemoteConnectConfig) *ssh.ClientConfig {
	hostKey, _, _, _, err := ssh.ParseAuthorizedKey([]byte(config.HostSSHKey))
	if err != nil {
		Logger.Printf("error parsing: %v", err)
	}

	signer := GetPublicCertificate(config.PrivateKeyPath)

	return &ssh.ClientConfig{
		User:            config.User,
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(signer)},
		HostKeyCallback: ssh.FixedHostKey(hostKey),
	}
}

// GetPublicCertificate retrieves it from the given privateKeyPath param
func GetPublicCertificate(privateKeyPath string) ssh.Signer {
	key, err := os.ReadFile(privateKeyPath)
	if err != nil {
		Logger.Printf("unable to read private key file: %v", err)
	}

	signer, err := ssh.ParsePrivateKey(key)
	if err != nil {
		Logger.Printf("Unable to parse private key: %v", err)
	}

	cert, _ := os.ReadFile(privateKeyPath + "-cert.pub")
	pk, _, _, _, err := ssh.ParseAuthorizedKey(cert)
	if err != nil {
		Logger.Printf("unable to parse CA public key: %v", err)
		return nil
	}

	certSigner, err := ssh.NewCertSigner(pk.(*ssh.Certificate), signer)
	if err != nil {
		Logger.Printf("failed to create cert signer: %v", err)
		return nil
	}

	return certSigner
}

func getDeployFingerprint(deployCertFilePath string) string {
	out, err := exec.Command("/usr/bin/ssh-keygen", "-Lf", deployCertFilePath).Output()
	if err != nil {
		Logger.Printf("ERR: %s", err.Error())
	}

	return string(out)
}

// WifiAction controls Wifi radio signal toggling (protect your sleep)
func WifiAction(param string) string {
	response := ":fritzbox: :wifi:\n"

	if param == "1" || param == "0" {
		response += execFritzCmd("WLAN", param)
	} else {
		response += execFritzCmd("WLAN", "STATE")
	}

	return response
}

func getIPv6forHostname(hostname string) string {
	cmd := "nslookup -type=aaaa " + hostname + " | grep Address | tail -n +2"
	domainIPv6Bytes, _ := exec.Command("/bin/sh", "-c", cmd).Output()

	domainIPv6 := string(bytes.Trim(domainIPv6Bytes, "\n"))
	domainIPv6 = strings.TrimPrefix(domainIPv6, "Address: ")

	return domainIPv6
}

func contains(arr []string, str string) bool {
	for _, a := range arr {
		if a == str {
			return true
		}
	}
	return false
}

// checkHomeFirewallSettings returns addresses of ackerson.de
// and internal network has inbound SSH access
func checkHomeFirewallSettings(domainIPv6 string, homeIPv6Prefix string) []string {
	authorizedIPs := []string{domainIPv6, homeIPv6Prefix, "192.168.178.0/24"}

	return retrieveHomeFirewallRules(authorizedIPs)
}

func fetchHomeIPs() (ipv6Prefix string, ipv4 string) {
	remoteResult := execFritzCmd("IGDIP", "STATE")

	re := regexp.MustCompile(`(.*)NewIPv6Prefix (?P<prefix>.*)\nNewPrefixLength (?P<length>.*)\n(.*)`)
	matches := re.FindAllStringSubmatch(remoteResult, -1)
	names := re.SubexpNames()

	m := map[string]string{}
	if len(matches) > 0 {
		for i, n := range matches[0] {
			m[names[i]] = n
		}
		if len(m) > 1 {
			ipv6Prefix = m["prefix"] + "/" + m["length"]
		}
	}

	re = regexp.MustCompile(`(.*)NewExternalIPAddress (?P<ipv4>.*)\n`)
	matches = re.FindAllStringSubmatch(remoteResult, -1)
	names = re.SubexpNames()

	m = map[string]string{}
	if len(matches) > 0 {
		for i, n := range matches[0] {
			m[names[i]] = n
		}
		if len(m) > 1 {
			ipv4 = m["ipv4"]
		}
	}
	return ipv6Prefix, ipv4
}

func dockerInfo(application string) string {
	response := ""
	cmd := "ssh homepage \"docker  logs -n 100 " + application + "\""
	if application == "" {
		cmd = "ssh homepage \"docker ps -a --format 'table {{.Names}}\t{{.Status}}'\""
	}

	remoteResult := executeRemoteCmd(cmd, structures.ChoreRemoteConnectConfig)
	if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
		response += remoteResult.Stderr
	} else {
		response += remoteResult.Stdout
	}

	return response
}

func execFritzCmd(action string, param string) string {
	response := ""

	cmd := fmt.Sprintf(
		"/home/ackersond/fritzBoxShell.sh %s %s", action, param)

	remoteResult := executeRemoteCmd(cmd, structures.ChoreRemoteConnectConfig)
	if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
		response = remoteResult.Stderr
	} else {
		response = remoteResult.Stdout
	}

	return response
}

func executeRemoteCmd(cmd string, config *structures.RemoteConnectConfig) structures.RemoteResult {
	defer func() { //catch or finally
		if err := recover(); err != nil { //catch
			Logger.Printf("ssh: connecting to `%s` threw -> %v\n", config.HostName, err)
		}
	}()

	remoteConfig := remoteConnectionConfiguration(config)
	if remoteConfig != nil && config.HostName != "" {
		sshClient := initialDialOut(config.HostName, remoteConfig)
		if sshClient != nil {
			session, err := sshClient.NewSession()
			if err != nil {
				Logger.Printf("unable to create SSH session: %s", err.Error())
				return structures.RemoteResult{
					Err:    err,
					Stdout: "",
					Stderr: "",
				}
			}
			defer session.Close()

			var stdoutBuf bytes.Buffer
			session.Stdout = &stdoutBuf
			var stderrBuf bytes.Buffer
			session.Stderr = &stderrBuf
			err = session.Run(cmd)

			errStr := ""
			if stderrBuf.String() != "" {
				errStr = strings.TrimSpace(stderrBuf.String())
			}

			return structures.RemoteResult{Err: err, Stdout: stdoutBuf.String(), Stderr: errStr}
		}
	}

	return structures.RemoteResult{}
}

func initialDialOut(hostname string, remoteConfig *ssh.ClientConfig) *ssh.Client {
	connectionString := fmt.Sprintf("%s:%s", hostname, "22")
	sshClient, errConn := ssh.Dial("tcp6", connectionString, remoteConfig)
	if errConn != nil { //catch
		Logger.Printf("ssh: Trying to connect to host `%s` failed w/ %s\n(%v)\n", connectionString, errConn.Error(), remoteConfig)
		return nil
	}

	return sshClient
}

// Slack gets excited when it recognizes a string that might be a URL
// e.g. de-16.protonvpn.com or Big.Buck.Bunny.2007.1080p.x264-[opensrc.org].mp4
// are sent to Bender as <http://de-16.protonvpn.com|de-16.protonvpn.com> or
// Big.Buck.Bunny.2007.1080p.x264-\[<http://opensrc.org|opensrc.org>\].mp4
func scrubParamOfHTTPMagicCrap(sourceString string) string {
	if strings.Contains(sourceString, "<http") {
		// strip out url tags leaving just the text
		re := regexp.MustCompile(`<http.*\|(.*)>`)
		sourceString = re.ReplaceAllString(sourceString, `$1`)
	}

	return sourceString
}

func raspberryPIChecks() string {
	response := measureCPUTemp()
	response += getAppVersions()

	return response
}

func getAppVersions() string {
	result := "\n*APPs* :martial_arts_uniform:\n"

	hosts := []structures.RemoteConnectConfig{
		*structures.CakeforRemoteConnectConfig,
		*structures.ChoreRemoteConnectConfig,
		*structures.TuxedoRemoteConnectConfig,
		*structures.ThorRemoteConnectConfig}

	for _, host := range hosts {
		result += "_" + host.HostName + "_: "
		remoteResult := executeRemoteCmd("docker --version", &host)
		result += remoteResult.Stdout
	}
	return result + "\n"
}

func measureCPUTemp() string {
	hosts := []structures.RemoteConnectConfig{
		*structures.CakeforRemoteConnectConfig,
		*structures.ChoreRemoteConnectConfig,
		*structures.TuxedoRemoteConnectConfig,
		*structures.ThorRemoteConnectConfig}

	result := "*CPUs* :thermometer:\n"
	for _, host := range hosts {
		// raspberryPIs
		measureCPUTempCmd := "((TEMP=`cat /sys/class/thermal/thermal_zone0/temp`/1000)); echo \"$TEMP\"C"

		remoteResult := executeRemoteCmd(measureCPUTempCmd, &host)
		if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
			result += "_" + host.HostName + "_: " + remoteResult.Stderr + "\n"
		} else {
			result += "_" + host.HostName + "_: *" + strings.TrimSuffix(remoteResult.Stdout, "\n") + "*\n"
		}
	}

	return result
}

func retrieveHomeFirewallRules(authorizedIPs []string) []string {
	hosts := []structures.RemoteConnectConfig{
		*structures.CakeforRemoteConnectConfig,
		*structures.ChoreRemoteConnectConfig,
		*structures.TuxedoRemoteConnectConfig,
		*structures.ThorRemoteConnectConfig}

	var result []string
	for _, host := range hosts {
		cmd := "/usr/bin/sudo /usr/sbin/ip6tables -L -n | grep :22$ | awk '{print $4}'"

		res := executeRemoteCmd(cmd, &host)
		Logger.Printf("res.Stdout: %v\n", res)
		if res.Stdout == "" {
			result = append(result, "Firewall is NOT enabled for "+host.HostName+"!")
		} else {
			scanner := bufio.NewScanner(strings.NewReader(res.Stdout))
			interim := ""
			for scanner.Scan() {
				if !contains(authorizedIPs, scanner.Text()) {
					interim += scanner.Text() + "\t"
				}
			}
			if interim != "" {
				result = append(result, host.HostName+": "+interim)
			}
		}
	}

	return result
}
