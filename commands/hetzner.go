package commands

import (
	"os"
	"strconv"
	"strings"

	"github.com/slack-go/slack"
	"gitlab.com/ackersonde/hetzner/hetznercloud"
)

func fetchExtraHetznerFirewallRules(homeIPv6Prefix, homeIPv4 string) []string {
	var extraRules []string

	sshFWRules := hetznercloud.GetSSHFirewallRules()
	for _, rule := range sshFWRules {
		if strings.TrimSpace(rule) != homeIPv6Prefix && strings.TrimSpace(rule) != homeIPv4+"/32" {
			Logger.Printf("%s doesn't MATCH %s\n", rule, homeIPv6Prefix)
			extraRules = append(extraRules, rule)
		}
	}

	return extraRules
}

// DisplayFirewallRules for daily cron
func DisplayFirewallRules() {
	openFirewallRules := checkFirewallRules(false)
	if openFirewallRules != "" {
		api.PostMessage(SlackReportChannel, slack.MsgOptionText(openFirewallRules, false),
			slack.MsgOptionAsUser(true))
	}
}

// checkFirewallRules does a cross check of SSH access between
// cloud instances and home network, ensuring minimal connectivity
func checkFirewallRules(manuallyCalled bool) string {
	homeIPv6Prefix, homeIPv4 := fetchHomeIPs()
	response := ""

	extras := fetchExtraHetznerFirewallRules(homeIPv6Prefix, homeIPv4)
	if len(extras) > 0 {
		response += ":htz_server: <https://console.hetzner.cloud/projects/" + os.Getenv("HETZNER_PROJECT") +
			"/firewalls/" + os.Getenv("HETZNER_FIREWALL") + "/rules|open to> -> " +
			"`" + strings.Join(extras, "`, `") + "`" + " :rotating_light:\n"
	} else if manuallyCalled {
		response += ":htz_server: allowed from `" + homeIPv6Prefix + "`,`" + homeIPv4 + "` :house:\n"
	}

	response += checkHetznerFirewallApplied()

	domainIPv6 := getIPv6forHostname("ackerson.de")
	homeFirewallRules := checkHomeFirewallSettings(domainIPv6, homeIPv6Prefix)
	if len(homeFirewallRules) > 0 {
		response += "\n:house: opened on -> `" + strings.Join(homeFirewallRules, "`, `") + "`" + " :rotating_light:"
	} else if manuallyCalled {
		response += "\n:house: allowed from `" + domainIPv6 + "` :htz_server:"
	}

	return strings.TrimSuffix(response, "\n")
}

func checkHetznerFirewallApplied() string {
	servers := hetznercloud.ListAllServers()

	id, _ := strconv.Atoi(os.Getenv("HETZNER_FIREWALL"))
	response := hetznercloud.ApplyFirewall(id, "label=docker")

	if strings.Contains(response, "firewall_already_applied") {
		return ""
	} else {
		return "\nApplying :fire:wall to <https://console.hetzner.cloud/projects/" + os.Getenv("HETZNER_PROJECT") + "/servers/" + strconv.Itoa(servers[0].ID) + "/firewalls|" + strconv.Itoa(servers[0].ID) + "> :rotating_light:\n"
	}
}
