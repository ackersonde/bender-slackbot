package commands

import (
	"bytes"
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"image/jpeg"
	"image/png"
	"net/http"
	"os"
	"strings"
	"time"

	vault "github.com/hashicorp/vault/api"
	auth "github.com/hashicorp/vault/api/auth/approle"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
	"gitlab.com/ackersonde/bender-slackbot/structures"
)

var totpVaultClient *vault.Client

func longVaultSession() {
	if totpVaultClient != nil && totpVaultClient.Token() != "" {
		return
	}

	go renewToken()
	loginAttempts := 0
	for loginAttempts < 10 {
		loginAttempts += 1
		if totpVaultClient == nil || totpVaultClient.Token() == "" {
			time.Sleep(100 * time.Millisecond)
		} else {
			break
		}
	}
}

func readTOTPCodeForKey(totpEngineName string, keyName string) string {
	longVaultSession()

	response := ""
	code, err := totpVaultClient.Logical().Read(totpEngineName + "/code/" + keyName)

	if err != nil {
		if strings.Contains(err.Error(), "unknown key: "+keyName) {
			allKeys := listTOTPKeysForEngine(totpEngineName)
			// search thru remoteResults and attempt to do a case-insensitive match on keyname
			for _, key := range allKeys {
				lowerCaseKey := strings.ToLower(key)
				lowerCaseKeyName := strings.ToLower(keyName)

				if strings.Contains(lowerCaseKey, lowerCaseKeyName) {
					response += key + ": " + readTOTPCodeForKey(totpEngineName, key) + "\n"
				}
			}
			if response == "" {
				response = fmt.Sprintf("Unable to find keyName %s. Try looking at all keys w/ `vfa`\n", keyName)
			} else {
				response = fmt.Sprintf("Found the following keys:\n%s\n", response)
			}
		} else {
			response = fmt.Sprintf("unexpected error: %s\n", err.Error())
		}
	} else {
		response = "*" + code.Data["code"].(string) + "*"
	}

	return response
}

func putTOTPKeyForEngine(totpEngineName string, keyName string, secret string) string {
	longVaultSession()
	response := ""

	payload := map[string]interface{}{
		"key": secret,
	}
	_, err := totpVaultClient.Logical().Write(totpEngineName+"/keys/"+keyName, payload)

	if err != nil {
		response = fmt.Sprintf("Unable to add %s: %s", keyName, err.Error())
	} else {
		response = readTOTPCodeForKey(totpEngineName, keyName)
	}

	return response
}

var imageKey = "dataB64"

func storeVaultImage(event *slackevents.MessageEvent, imageName string) string {
	response := ""
	if len(event.Files) <= 0 || imageName == "" {
		response = "Please try again with an uploaded image and name\n"
	} else {
		filepath := os.TempDir() + string(os.PathSeparator) + event.ClientMsgID + "-" + event.Files[0].Name
		file, err := os.Create(filepath)

		if err != nil {
			response = fmt.Sprintf("Error opening file: %s (%s)\n", filepath, err)
		} else {
			err = api.GetFile(event.Files[0].URLPrivateDownload, file)
			Logger.Printf("downloading %s to %s\n", event.Files[0].URLPrivateDownload, filepath)
			if err != nil {
				Logger.Println("error downloading file: ", err)
			}
			defer file.Close()

			bytes, err := os.ReadFile(filepath)
			if err != nil {
				response = fmt.Sprintf("Error reading file: %s (%s)\n", filepath, err)
			} else {
				base64Image := "data:" + http.DetectContentType(bytes) + ";base64," + base64.StdEncoding.EncodeToString(bytes)
				secret := make(map[string]interface{})
				secret["data"] = map[string]interface{}{
					imageKey: base64Image,
				}

				vaultClient := singleVaultLogin("git-secrets-ackersonde-mgmt")
				_, err := vaultClient.Logical().Write("secret/photos/data/"+imageName, secret)
				if err != nil {
					response = fmt.Sprintf("Unable to save image %s: %s\n", imageName, err.Error())
				} else {
					response = fmt.Sprintf("Wrote %s to Vault\n", imageName)
				}
			}
		}

		// cleanup after ourselves
		err = os.Remove(filepath)
		if err != nil {
			Logger.Println("error removing file: ", err)
		}
	}

	return response
}

func retrieveVaultImage(api *slack.Client, event *slackevents.MessageEvent, imageName string) string {
	response := ""
	vaultClient := singleVaultLogin("git-secrets-ackersonde-mgmt")

	secret, err := vaultClient.Logical().Read("secret/photos/data/" + imageName)
	if err != nil {
		response = fmt.Sprintf("Unable to retrieve image %s: %s\n", imageName, err.Error())
	} else if secret != nil {
		// read the base64 file, decode and resave it to new file
		value, ok := secret.Data["data"].(map[string]interface{})
		if !ok {
			Logger.Printf("%T %#v\n", secret.Data["data"], secret.Data["data"])
		} else {
			// https://stackoverflow.com/questions/46022262/convert-base64-string-to-jpg
			secretData := value[imageKey].(string)
			coI := strings.Index(secretData, ",")
			rawImage := secretData[coI+1:]

			unbased, _ := base64.StdEncoding.DecodeString(string(rawImage))
			res := bytes.NewReader(unbased)

			var f *os.File
			fileName := imageName
			path := os.TempDir() + string(os.PathSeparator)

			switch strings.TrimSuffix(secretData[5:coI], ";base64") {
			case "image/png":
				fileName += ".png"
				pngI, err := png.Decode(res)
				if err != nil {
					return fmt.Sprintf("Error decoding png: %s\n", err)
				} else {
					path := os.TempDir() + string(os.PathSeparator) + fileName
					f, err = os.Create(path)
					if err != nil {
						return fmt.Sprintf("Error opening file: %s", err.Error())
					}
					err = png.Encode(f, pngI)
					if err != nil {
						return fmt.Sprintf("Error encoding jpeg: %s", err.Error())
					}
				}

			case "image/jpeg":
				fileName += ".jpg"
				jpgI, err := jpeg.Decode(res)
				if err != nil {
					return fmt.Sprintf("Error decoding jpeg: %s\n", err)
				} else {
					path += fileName
					f, err = os.Create(path)
					if err != nil {
						return fmt.Sprintf("Error opening file: %s", err.Error())
					}
					err = jpeg.Encode(f, jpgI, nil)
					if err != nil {
						return fmt.Sprintf("Error encoding jpeg: %s", err.Error())
					}
				}
			}

			if f != nil {
				defer f.Close()
				fi, _ := f.Stat()
				_, err = api.UploadFileV2(slack.UploadFileV2Parameters{
					Channel:         event.Channel,
					File:            path,
					Filename:        fileName,
					Title:           fileName,
					FileSize:        int(fi.Size()),
					ThreadTimestamp: event.TimeStamp,
				})
				if err != nil {
					return fmt.Sprintf("Error uploading file: %s\n", err)
				}
				response = fmt.Sprintf("Here ya go, %s:", event.Message.User)
			}
		}
	} else {
		response = fmt.Sprintf("Image %s not found\n", imageName)
	}

	return response
}

func fetchVaultImages() string {
	response := ""
	vaultClient := singleVaultLogin("git-secrets-ackersonde-mgmt")

	images, err := vaultClient.Logical().List("secret/photos/metadata")
	if err != nil {
		response = fmt.Sprintf("Unable to list images: %s\n", err.Error())
	} else {
		for _, image := range images.Data["keys"].([]interface{}) {
			response += image.(string) + "\n"
		}
	}

	return response
}

func updateRoleCIDRs(authRole string, updateRole string, CIDRs string) string {
	updateVaultClient := totpVaultClient

	if authRole == "totp-mgmt" {
		longVaultSession()
	} else {
		updateVaultClient = singleVaultLogin(authRole)
	}

	response := ""

	parts := strings.Split(CIDRs, ",")
	finalList := ""
	for _, part := range parts {
		finalList += part + ","
	}
	finalList += "172.19.0.0/24"

	payload := map[string]interface{}{
		"token_bound_cidrs": finalList,
	}
	_, err := updateVaultClient.Logical().Write("auth/approle/role/"+updateRole, payload)
	if err != nil {
		response = fmt.Sprintf("Unable to update %s's CIDRs to %s: %s\n", updateRole, finalList, err.Error())
	} else {
		response = fmt.Sprintf("Updated %s's CIDRS to `%s`\n", updateRole, CIDRs)
	}
	return response
}

func listTOTPKeysForEngine(totpEngineName string) []string {
	longVaultSession()
	var response []string
	keys, err := totpVaultClient.Logical().List(totpEngineName + "/keys")

	if err != nil {
		response = append(response, err.Error())
	} else {
		for _, key := range keys.Data["keys"].([]interface{}) {
			response = append(response, key.(string))
		}
	}

	return response
}

func singleVaultLogin(role string) *vault.Client {
	vaultClientLogin, _ := vault.NewClient(vault.DefaultConfig())
	vaultClientLogin.SetAddress(os.Getenv("VAULT_ADDR"))

	_, err := loginToVault(vaultClientLogin, role)

	if err != nil {
		Logger.Printf("unable to authenticate to Vault: %v\n", err)
	}

	return vaultClientLogin
}

// The Secret ID is a value that needs to be protected, so instead of the
// app having knowledge of the secret ID directly,
func loginToVault(vaultClientLogin *vault.Client, role string) (*vault.Secret, error) {
	// A combination of a Role ID and Secret ID is required to log in to Vault
	// with an AppRole.
	// First, let's get the role ID given to us by our Vault administrator.
	role = strings.ReplaceAll(role, "-", "_")
	roleID := os.Getenv("VAULT_" + role + "_APPROLE") // git_secrets_ackersonde_mgmt
	if roleID == "" {
		return nil, errors.New("no role ID was provided in VAULT_" + role + "_APPROLE env var")
	}

	// The Secret ID is a value that needs to be protected, so instead of the
	// app having knowledge of the secret ID directly, we have a trusted orchestrator (https://learn.hashicorp.com/tutorials/vault/secure-introduction?in=vault/app-integration#trusted-orchestrator)
	// give the app access to a short-lived response-wrapping token (https://www.vaultproject.io/docs/concepts/response-wrapping).
	// Read more at: https://learn.hashicorp.com/tutorials/vault/approle-best-practices?in=vault/auth-methods#secretid-delivery-best-practices
	secretID := &auth.SecretID{FromString: os.Getenv("VAULT_" + role + "_SECRET")}

	appRoleAuth, err := auth.NewAppRoleAuth(roleID, secretID)
	if err != nil {
		Logger.Println(fmt.Errorf("unable to initialize AppRole auth method: %w", err))
	} else {
		authInfo, err := vaultClientLogin.Auth().Login(context.Background(), appRoleAuth)
		if err != nil {
			Logger.Println(fmt.Errorf("%w", err))
			if strings.Contains(err.Error(), "Vault is sealed") {
				cmd := "/home/ackersond/vault/unseal_vault.sh"
				remoteResult := executeRemoteCmd(cmd, structures.ChoreRemoteConnectConfig)

				if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
					Logger.Println(remoteResult.Stderr)
					err = errors.New(remoteResult.Stderr)
				} else {
					time.Sleep(5 * time.Second)
					authInfo, err = vaultClientLogin.Auth().Login(context.Background(), appRoleAuth)
				}
			}
		}

		if authInfo == nil {
			Logger.Printf("no auth info was returned after login")
		}

		return authInfo, err
	}

	return nil, err
}

// Once you've set the token for your Vault client, periodically renew lease
func renewToken() {
	if totpVaultClient == nil {
		totpVaultClient, _ = vault.NewClient(vault.DefaultConfig())
		totpVaultClient.SetAddress(os.Getenv("VAULT_ADDR"))
	}

	for {
		vaultLoginResp, err := loginToVault(totpVaultClient, "totp-mgmt")
		if err != nil {
			Logger.Printf("unable to authenticate to Vault: %v\n", err)
			break
		} else {
			tokenErr := manageTokenLifecycle(vaultLoginResp)
			if tokenErr != nil {
				Logger.Printf("unable to start managing token lifecycle: %v\n", tokenErr)
				break
			}
		}
	}
}

// Starts token lifecycle management. Returns only fatal errors as errors,
// otherwise returns nil so we can attempt login again.
func manageTokenLifecycle(token *vault.Secret) error {
	renew := token.Auth.Renewable // You may notice a different top-level field called Renewable. That one is used for dynamic secrets renewal, not token renewal.
	if !renew {
		Logger.Printf("Token is not configured to be renewable. Re-attempting login.")
		return nil
	}

	watcher, err := totpVaultClient.NewLifetimeWatcher(&vault.LifetimeWatcherInput{
		Secret:    token,
		Increment: 3600, // Learn more about this optional value in https://www.vaultproject.io/docs/concepts/lease#lease-durations-and-renewal
	})
	if err != nil {
		return fmt.Errorf("unable to initialize new lifetime watcher for renewing auth token: %w", err)
	}

	go watcher.Start()
	defer watcher.Stop()

	for {
		select {
		// `DoneCh` will return if renewal fails, or if the remaining lease
		// duration is under a built-in threshold and either renewing is not
		// extending it or renewing is disabled. In any case, the caller
		// needs to attempt to log in again.
		case err := <-watcher.DoneCh():
			if err != nil {
				Logger.Printf("Failed to renew token: %v. Re-attempting login.", err)
				return nil
			}
			// This occurs once the token has reached max TTL.
			Logger.Printf("Token can no longer be renewed. Re-attempting login.")
			return nil

		// Successfully completed renewal
		case renewal := <-watcher.RenewCh():
			duration, _ := renewal.Secret.TokenTTL()
			Logger.Printf("Renewed vault Token for addtl %s", duration)
		}
	}
}
