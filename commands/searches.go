package commands

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/StalkR/imdb"
	"github.com/kris-nova/photoprism-client-go"
	"gitlab.com/ackersonde/bender-slackbot/structures"
)

var proxies = []string{"apibay.org"}

func searchProxy(url string) []byte {
	var jsonResults []byte

	for i, proxy := range proxies {
		uri := "https://" + proxy + url
		Logger.Printf("torq try #%d: %s ...\n", i, uri)
		req, err := http.NewRequest("GET", uri, nil)
		if err != nil {
			Logger.Printf("http.NewRequest() failed with '%s'\n", err)
			continue
		}

		// create a context indicating 5s timeout
		ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond*5000)
		defer cancel()
		req = req.WithContext(ctx)
		resp, err := http.DefaultClient.Do(req)
		if err != nil || resp.StatusCode != http.StatusOK {
			if err != nil {
				Logger.Printf("%s failed with:\n'%s'\n", proxy, err)
			} else {
				Logger.Printf("GET %s failed with '%s'\n", uri, resp.Status)
			}
			continue
		}
		if resp != nil {
			defer resp.Body.Close()
			body, err := io.ReadAll(resp.Body)
			if err != nil || body == nil {
				Logger.Printf("failed to parse JSON: %s", err.Error())
				continue
			}

			return body
		}
	}

	return jsonResults
}

func parseTop100(jsonResponse []byte) string {
	return top100Response(*getTop100FromJSON(jsonResponse))
}

func parseTorrents(jsonResponse []byte) string {
	return torrentResponse(*getTorrentsFromJSON(jsonResponse))
}

func parseMovieTorrents(args []string) string {
	results := ""

	// TODO: grab top 30 downloaded movies (hint:`sort_by=seeds` is broken 23.06.2023)
	uri := "https://yts.mx/api/v2/list_movies.json?sort_by=download_count&limit=50"

	if len(args) > 0 {
		uri += "&query_term=" + strings.Join(args, "+")
	}
	req, err := http.NewRequest("GET", uri, nil)
	if err != nil {
		Logger.Printf("YTS.MX http.NewRequest() failed with '%s'\n", err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil || resp.StatusCode != http.StatusOK {
		if err != nil {
			Logger.Printf("YTS.MX/api failed with:\n'%s'\n", err)
		} else {
			Logger.Printf("GET %s failed with '%s'\n", uri, resp.Status)
		}
	}

	if resp != nil {
		defer resp.Body.Close()
		body, err := io.ReadAll(resp.Body)
		if err != nil || body == nil {
			Logger.Printf("failed to parse JSON: %s", err.Error())
		}

		var ytsMxTopMovies = new(structures.YtsMx)
		marshalErr := json.Unmarshal(body, &ytsMxTopMovies)
		if marshalErr != nil {
			Logger.Printf("ERR: %s", marshalErr)
		}
		for i, movie := range ytsMxTopMovies.Data.Movies {
			torrentsFound := false
			for _, torrent := range movie.Torrents {
				if torrent.Seeds > 20 && (torrent.Quality == "1080p" || torrent.Quality == "2160p") {
					if !torrentsFound {
						results += fmt.Sprintf("%d: *%s %d* (<https://www.imdb.com/title/%s|imdb>)\n", i, movie.Title, movie.Year, movie.ImdbCode)
						torrentsFound = true
					}
					results += prepareMultiLinks(torrent.Hash, torrent.Seeds, torrent.Size, torrent.Quality)
				}
			}
		}
	}

	return fmt.Sprintf("YTS.MX search for '%s':\n%s", args, results)
}

func parseTVTorrents(args []string) string {
	results := ""
	uri := ""
	// EZ.tv doesn't have query search
	// So we query IMDB for the show, then use the IMDB ID to search EZ.tv
	if len(args) > 0 {
		client := &http.Client{
			Transport: &customTransport{http.DefaultTransport},
		}

		titles, err := imdb.SearchTitle(client, strings.Join(args, " "))
		if err != nil {
			return fmt.Sprintf("Error 1: %s: %v\n", args, err)
		}
		if len(titles) == 0 {
			return fmt.Sprintf("%v not found.", titles)
		}

		for _, title := range titles {
			if strings.HasPrefix(title.Type, "TV") {
				t, err := imdb.NewTitle(client, title.ID)
				if err != nil {
					return fmt.Sprintf("Error 2: %v\n", err)
				}

				uri = "https://eztv.re/api/get-torrents?imdb_id=" + strings.TrimPrefix(t.ID, "tt")
				req, err := http.NewRequest("GET", uri, nil)
				if err != nil {
					Logger.Printf("ez.tv/api http.NewRequest() failed with '%s'\n", err)
				}

				resp, err := http.DefaultClient.Do(req)
				if err != nil || resp.StatusCode != http.StatusOK {
					if err != nil {
						Logger.Printf("ez.tv/api failed with:\n'%s'\n", err)
					} else {
						Logger.Printf("GET %s failed with '%s'\n", uri, resp.Status)
					}
				}

				if resp != nil {
					defer resp.Body.Close()
					body, err := io.ReadAll(resp.Body)
					if err != nil || body == nil {
						Logger.Printf("failed to parse JSON: %s", err.Error())
					}

					var EZTvShows = new(structures.EzTv)
					marshalErr := json.Unmarshal(body, &EZTvShows)
					if marshalErr != nil {
						Logger.Printf("ERR: %s", marshalErr)
					}

					for i, show := range EZTvShows.Torrents {
						if show.Seeds > 50 {
							size, _ := strconv.ParseUint(show.SizeBytes, 10, 64)
							results += prepareLink(i, show.Hash, show.Title, show.Seeds, calculateHumanSize(size), show.ImdbID, "")
						}
					}
				}
			}
		}
	} else {
		// query IMDB for top 20 tv shows?
		results = "Top 20 IMDB ???"
	}

	// Now, use the ID (minus the 'tt' !) to search eztv:
	return fmt.Sprintf("ez.tv search for '%s':\n%s", args, results)
}

func top100Response(top100 structures.Top100Movies) string {
	var response string

	for i, torrent := range top100 {
		if torrent.Seeders > 10 {
			if torrent.Imdb == nil {
				torrent.Imdb = ""
			}
			response += prepareLink(
				i, torrent.InfoHash, torrent.Name,
				torrent.Seeders, calculateHumanSize(torrent.Size),
				torrent.Imdb.(string), "")
		}
	}

	if response == "" {
		return "Unable to find torrents for your search"
	}

	return response
}

func torrentResponse(torrents structures.Torrents) string {
	var response string

	for i, torrent := range torrents {
		seeders, err2 := strconv.Atoi(torrent.Seeders)
		if err2 != nil {
			Logger.Printf("ERR torrent seeder Atoi: %s\n", err2.Error())
			continue
		}
		size, err3 := strconv.ParseUint(torrent.Size, 10, 64)
		if err3 != nil {
			Logger.Printf("ERR torrent size Atoi: %s\n", err3.Error())
			continue
		}

		if seeders > 10 {
			response += prepareLink(
				i, torrent.InfoHash, torrent.Name,
				seeders, calculateHumanSize(size), torrent.Imdb, "")
		}
	}

	if response == "" {
		return "Unable to find torrents for your search"
	}
	return response
}

func calculateHumanSize(size uint64) string {
	humanSize := float64(size / (1024 * 1024))
	sizeSuffix := fmt.Sprintf("*%.0f MiB*", humanSize)
	if humanSize > 999 {
		humanSize = humanSize / 1024
		sizeSuffix = fmt.Sprintf("*%.1f GiB*", humanSize)
	}
	return sizeSuffix
}

func prepareMultiLinks(magnetLink string, torrentSeeders int,
	sizeSuffix string, quality string) string {
	var response string

	magnetLink = fmt.Sprintf("magnet/?xt=urn:btih:%s", magnetLink)
	response += fmt.Sprintf(
		" - <http://%s|%s> Seeds:%d %s", magnetLink, quality, torrentSeeders, sizeSuffix)

	return response + "\n"
}

func prepareLink(i int, magnetLink string, torrentName string,
	torrentSeeders int, sizeSuffix string, imdb string, quality string) string {
	var response string

	magnetLink = fmt.Sprintf("magnet/?xt=urn:btih:%s", magnetLink)
	response += fmt.Sprintf(
		"%d: <http://%s|%s> Seeds:%d %s %s", i, magnetLink,
		torrentName, torrentSeeders, quality, sizeSuffix)

	if imdb != "" {
		response += fmt.Sprintf(" (<https://www.imdb.com/title/%s|imdb>)", imdb)
	}

	return response + "\n"
}

func getTorrentsFromJSON(jsonObject []byte) *structures.Torrents {
	var s = new(structures.Torrents)
	err := json.Unmarshal(jsonObject, &s)
	if err != nil {
		Logger.Printf("ERR: %s => %s", err, jsonObject)
	}

	return s
}

func getTop100FromJSON(jsonObject []byte) *structures.Top100Movies {
	var s = new(structures.Top100Movies)
	err := json.Unmarshal(jsonObject, &s)
	if err != nil {
		Logger.Printf("ERR: %s => %s", err, jsonObject)
	}

	return s
}

func buildPhotoPrismAlbums() string {
	populatedAlbumsResponse := "Couldn't find any albums :/"
	client := photoprism.New(os.Getenv("PHOTOS_DOMAIN"))

	//client := photoprism.New(os.Getenv("PHOTOS_DOMAIN"), os.Getenv("PHOTOS_PASSWORD"), os.Getenv("PHOTOS_PASSWORD"))
	client.V1().SetToken(os.Getenv("PHOTOS_PASSWORD"))

	albums, err := client.V1().GetAlbums(nil)
	if err != nil {
		Logger.Printf("Unable to get albums...%s", err.Error())
	}

	for _, album := range albums {
		Logger.Printf("Album %s: %v", album.AlbumUID, album.AlbumSlug)
		var links structures.PhotoPrismLinks
		client.V1().GET("%s", "/api/v1/albums/"+album.AlbumUID+"/links").JSON(&links)

		if len(links) > 0 {
			expirationDate := links[0].ModifiedAt.AddDate(0, 0, -1*links[0].Expires)
			if links[0].Expires == 0 || !(time.Now().After(expirationDate)) {
				expiringInDays := links[0].Expires / 3600 / 24
				expiryMessage := fmt.Sprintf("expiring %dd w/ ", expiringInDays)
				if expiringInDays == 0 {
					expiryMessage = ""
				}

				publicURL := fmt.Sprintf("%s/s/%s", os.Getenv("PHOTOS_DOMAIN"), links[0].Token)
				views := links[0].Views

				populatedAlbumsResponse += fmt.Sprintf("<%s|%s> (%s%d views)\n", publicURL, album.AlbumSlug, expiryMessage, views)
			}
		}
	}

	return populatedAlbumsResponse
}

// IMDb deployed awswaf and denies requests using the default Go user-agent (Go-http-client/1.1).
// For now it still allows requests from a browser user-agent. Remain respectful, no spam, etc.
const userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36"

type customTransport struct {
	http.RoundTripper
}

func (e *customTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	defer time.Sleep(time.Second)         // don't go too fast or risk being blocked by awswaf
	r.Header.Set("Accept-Language", "en") // avoid IP-based language detection
	r.Header.Set("User-Agent", userAgent)
	return e.RoundTripper.RoundTrip(r)
}
