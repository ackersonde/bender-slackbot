package commands

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"image"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"

	jsoniter "github.com/json-iterator/go"
	"github.com/sashabaranov/go-openai"
	"github.com/sethvargo/go-password/password"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
	"gitlab.com/ackersonde/bender-slackbot/structures"
	"gitlab.com/ackersonde/hetzner/hetznercloud"
)

var api *slack.Client
var gitlabRunID = os.Getenv("CI_PIPELINE_ID")
var HETZNER_PROJECT = os.Getenv("HETZNER_PROJECT")

// Logger to give senseful settings
var Logger = log.New(os.Stdout, "", log.LstdFlags)

// VPN default connection (double check against https://account.protonvpn.com/downloads)
var defaultVPNpiTunnel = "NL_15"

// Syncthing directory
var syncthing = "/app/sync/"

// SlackReportChannel default reporting channel for bot crons
var SlackReportChannel = os.Getenv("SLACK_CHANNEL")

// SetAPI sets singleton
func SetAPI(apiPassed *slack.Client) {
	api = apiPassed
}

// CheckCommand is now commented
func CheckCommand(event *slackevents.MessageEvent, user *slack.User, command string) {
	args := strings.Fields(command)
	params := slack.MsgOptionAsUser(true)

	if args[0] == "h4X07s" {
		checkHost := "homepage"
		host := ":vault: ackerson.de"
		if len(args) > 1 {
			checkHost = args[1]
			if checkHost == "chore" {
				host = ":home: ackerson.de"
			}
		}
		response := ""
		cmdPrefix := "ssh " + checkHost + " "

		// grab Better Stack IPs
		cmd := fmt.Sprintf("%s\"curl https://uptime.betterstack.com/ips.txt -o /tmp/ips.txt\"", cmdPrefix)
		executeRemoteCmd(cmd, structures.ChoreRemoteConnectConfig)

		homeIPv6Prefix, homeIPv4 := fetchHomeIPs()

		ipv6Prefix := strings.Split(homeIPv6Prefix, ":")
		homeIPv6 := ipv6Prefix[0] + ":" + ipv6Prefix[1] + ":" + string(ipv6Prefix[2][0])
		internalIPs := "| grep -v " + homeIPv6 + " | grep -v " + homeIPv4 + " | grep -v 192.168. | grep -v 172.18. | grep -v 172.19."

		// grab access.log without local prefix requests
		// if prefix is e.g. "2a02:8109:8c0:1c0:1::", then filter with "2a02:8109:8" to remove local IPs
		cmd = fmt.Sprintf("%s\"awk '{print \\$1}' traefik/logs/access.log %s > /tmp/out.txt\"",
			cmdPrefix, internalIPs)
		executeRemoteCmd(cmd, structures.ChoreRemoteConnectConfig)

		// remove Better Stack IPs from filtered access.log
		cmd = fmt.Sprintf("%s\"grep -Fxv -f /tmp/ips.txt /tmp/out.txt > /tmp/hmm.out\"", cmdPrefix)
		executeRemoteCmd(cmd, structures.ChoreRemoteConnectConfig)

		cmd = fmt.Sprintf("%s\"awk '{print \\$1}' /tmp/hmm.out | sort -n | uniq -c | sort -nr | head -10\"", cmdPrefix)
		remoteResult := executeRemoteCmd(cmd, structures.ChoreRemoteConnectConfig)
		if remoteResult.Stdout == "" && remoteResult.Stderr != "" {
			response = "=======" + host + "==========\n" + remoteResult.Stderr
		} else {
			response = "=======" + host + "==========\n" + remoteResult.Stdout
		}

		// TODO: iptable rules for blocking IPs from DOCKER-USER chain
		// e.g. sudo iptables -t mangle -A PREROUTING -i eth0 -s 114.119.128.0/18 -j DROP
		// because blocking directly on the INPUT chain doesn't work for Docker containers!
		api.PostMessage(event.Channel,
			slack.MsgOptionText(response, false), params)
	} else if args[0] == "crypto" {
		response := checkEthereumValue() + "\n" + checkStellarLumensValue()
		api.PostMessage(event.Channel,
			slack.MsgOptionText(response, false), params)
	} else if args[0] == "pgp" {
		api.PostMessage(event.Channel,
			slack.MsgOptionText(pgpKeys(), false), params)
	} else if args[0] == "pi" {
		api.PostMessage(event.Channel,
			slack.MsgOptionText(raspberryPIChecks(), false), params)
	} else if args[0] == "wf" {
		if len(args) < 2 {
			args = append(args, "STATE") // empty cmd shows wifi status
		}
		api.PostMessage(event.Channel,
			slack.MsgOptionText(WifiAction(args[1]), false), params)
	} else if args[0] == "yt" {
		if len(args) > 1 {
			downloadURL := strings.Trim(scrubParamOfHTTPMagicCrap(args[1]), "<>")
			Logger.Printf("downloadURL: %s", downloadURL)
			uri, err := url.ParseRequestURI(downloadURL)
			Logger.Printf("parsed %s from %s", uri.RequestURI(), downloadURL)
			if err != nil {
				api.PostMessage(event.Channel,
					slack.MsgOptionText(
						"Invalid URL for downloading! ("+err.Error()+")", true), params)
			} else {
				_, err := exec.Command("/usr/bin/yt-dlp", uri.String(),
					"-o", syncthing+"%(title)s.%(ext)s").Output()
				if err == nil {
					api.PostMessage(event.Channel,
						slack.MsgOptionText(
							"Requested YouTube video. Check your phone in a few minutes...", true), params)
				} else {
					api.PostMessage(event.Channel,
						slack.MsgOptionText(
							"Unable to download YouTube video..."+err.Error(), true), params)
				}
			}
		} else {
			api.PostMessage(event.Channel,
				slack.MsgOptionText("Please provide YouTube video URL!", true), params)
		}
	} else if args[0] == "bb" {
		result := ""
		dateString := ""

		if len(args) > 1 {
			gameDate, err := time.Parse("2006-01-02", args[1])
			dateString = gameDate.Format("2006/month_01/day_02")

			if err != nil {
				result = "Couldn't figure out date '" + args[1] + "'. Try `help`"
				api.PostMessage(event.Channel, slack.MsgOptionText(result, false), params)
				return
			}
		}
		result = ShowBBGames(dateString)
		api.PostMessage(event.Channel, slack.MsgOptionText(result, false), params)
	} else if args[0] == "logs" {
		result := "Unable to query docker..."
		if len(args) > 1 {
			result = dockerInfo(args[1])
		} else {
			result = dockerInfo("")
		}
		api.PostMessage(event.Channel, slack.MsgOptionText(result, false), params)
	} else if args[0] == "pass" {
		usage := "Usage: pass <64 10 10 false true>\nparams are <chars:64 digits:10 symbols:10 noUpper:false allowRepeat:true>.\nOrder counts! If you only need to change `noUpper`, you *must* enter preceding params."
		response := ""
		var err error

		chars := 64
		digits := 10
		symbols := 10

		switch len(args) {
		case 2:
			if strings.HasPrefix(args[1], "-") &&
				(strings.HasSuffix(args[1], "h") || strings.HasSuffix(args[1], "help")) {
				response = usage
			} else {
				chars, _ = strconv.Atoi(args[1])

				if chars < 20 {
					digits = chars / 2
					symbols = digits
				}
				response, err = password.Generate(chars, digits, symbols, false, false)
				if err != nil {
					response = err.Error() + "\n" + usage
				}
			}
		case 3:
			chars, _ := strconv.Atoi(args[1])
			digits, _ := strconv.Atoi(args[2])

			if chars-digits > 10 {
				symbols = chars - digits
			}
			response, err = password.Generate(chars, digits, symbols, false, false)
			if err != nil {
				response = err.Error() + "\n" + usage
			}
		case 4:
			chars, _ := strconv.Atoi(args[1])
			digits, _ := strconv.Atoi(args[2])
			symbols, _ := strconv.Atoi(args[3])

			response, err = password.Generate(chars, digits, symbols, false, false)
			if err != nil {
				response = err.Error() + "\n" + usage
			}
		case 5:
			chars, _ := strconv.Atoi(args[1])
			digits, _ := strconv.Atoi(args[2])
			symbols, _ := strconv.Atoi(args[3])
			upAndLow, _ := strconv.ParseBool(args[4])

			response, err = password.Generate(chars, digits, symbols, upAndLow, false)
			if err != nil {
				response = err.Error() + "\n" + usage
			}
		case 6:
			chars, _ := strconv.Atoi(args[1])
			digits, _ := strconv.Atoi(args[2])
			symbols, _ := strconv.Atoi(args[3])
			upAndLow, _ := strconv.ParseBool(args[4])
			repeatChars, _ := strconv.ParseBool(args[5])

			response, err = password.Generate(chars, digits, symbols, upAndLow, !repeatChars)
			if err != nil {
				response = err.Error() + "\n" + usage
			}
		default:
			response, err = password.Generate(64, 10, 10, false, false)
			if err != nil {
				response = err.Error() + "\n" + usage
			}
		}

		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "vcu" {
		homeIPv6Prefix, _ := fetchHomeIPs()

		response := fmt.Sprintf("%s\n", updateRoleCIDRs("git-secrets-ackersonde-mgmt", "github-ackersonde-read", homeIPv6Prefix))
		response += fmt.Sprintf("%s\n", updateRoleCIDRs("git-secrets-ackersonde-mgmt", "github-ackersonde-write", homeIPv6Prefix))
		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "vfa" {
		response := "Usage: vfa <(get) keyname | put keyname secret> only in DM to <https://app.slack.com/client/T092UA8PR/D01D44R3N6L|@bender>"

		totpEngineName := "totp"
		if event.User != "U092UC9EW" {
			totpEngineName = "liuda"
		}

		homeIPv6Prefix, _ := fetchHomeIPs()
		if len(args) == 3 && args[1] == "update" {
			response = fmt.Sprintf("%s\n", updateRoleCIDRs("single", "totp-mgmt",
				homeIPv6Prefix))
		} else if strings.HasPrefix(event.Channel, "D") { // only process token reqs in DMs (no leaking of 2FA codes in public channels!)
			if len(args) == 1 || (len(args) == 2 && args[1] == "get") {
				response = fmt.Sprintf("%v\n", listTOTPKeysForEngine(totpEngineName))
			} else if args[1] != "put" {
				keyname := args[1]
				if args[1] == "get" {
					keyname = args[2]
				}
				response = fmt.Sprintf("%s\n", readTOTPCodeForKey(totpEngineName, keyname))
			} else if args[1] == "put" {
				response = fmt.Sprintf("%s\n", putTOTPKeyForEngine(totpEngineName, args[2], args[3]))
			}
		}

		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "htz" {
		response := "No servers at :htz_server:."
		servers := hetznercloud.ListAllServers()
		if len(servers) > 0 {
			response = "Found following server(s) at :htz_server::\n"
		}

		for _, server := range servers {
			serverInfoURL := fmt.Sprintf("https://console.hetzner.cloud/projects/%s/servers/%d/graphs", HETZNER_PROJECT, server.ID)
			serverIPv6 := server.PublicNet.IPv6.IP.String()
			if strings.HasSuffix(serverIPv6, "::") {
				serverIPv6 += "1"
			}

			response += fmt.Sprintf("ID %d: <%s|%s> [%s] @ %s => %s\n",
				server.ID, serverInfoURL, server.Name, serverIPv6,
				server.Created.Format("2006-01-02 15:04"), server.Status)

			serverNameParts := strings.Split(server.Name, "-")
			remoteCmd := fmt.Sprintf("ssh %s 'uptime;uname -a'", serverNameParts[0])
			remoteResult := executeRemoteCmd(remoteCmd, structures.ChoreRemoteConnectConfig)
			response += remoteResult.Stdout
		}

		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "htzd" {
		if len(args) > 1 {
			serverID, err := strconv.Atoi(args[1])
			if err != nil {
				api.PostMessage(event.Channel, slack.MsgOptionText("Invalid integer value for ID!", true), params)
			} else {
				if event.User == "U092UC9EW" {
					result := hetznercloud.DeleteServer(serverID)
					api.PostMessage(event.Channel, slack.MsgOptionText(result, true), params)
				} else {
					api.PostMessage(event.Channel, slack.MsgOptionText("Only Dan can delete servers!", true), params)
				}
			}
		} else {
			api.PostMessage(event.Channel, slack.MsgOptionText("Please provide Server ID from `htz` cmd!", true), params)
		}
	} else if args[0] == "fsck" {
		response := ""
		if len(args) > 1 {
			path := strings.Join(args[1:], " ")
			response += CheckMediaDiskSpace(path)
			response += CheckServerDiskSpace("")
		} else {
			response += CheckMediaDiskSpace("")
			response += CheckServerDiskSpace("")
		}

		response += ":htz_server: *Hetzner Disk Usage* @ `/`:\n"
		response += CheckHetznerSpace("/", true)

		response += "\n==========================\n:floppy_disk: *Remote Storage and Backups issues*\n"
		issues := CheckDiskSpace(true)
		issues += CheckBackups(true)
		if issues == "" {
			issues = ":simple_smile: None found :rainbow:"
		} else {
			issues = ":skull_and_crossbones: " + issues + " :skull_and_crossbones:"
		}
		response += issues

		api.PostMessage(event.Channel, slack.MsgOptionText(response, true), params)
	} else if args[0] == "mv" {
		if len(args) == 3 &&
			(strings.HasPrefix(args[2], "movies") ||
				strings.HasPrefix(args[2], "tv")) {
			sourceFile := scrubParamOfHTTPMagicCrap(args[1])
			destinationDir := args[2]
			if strings.Contains(destinationDir, "..") || strings.HasPrefix(destinationDir, "/") {
				msg := fmt.Sprintln("Please prefix destination w/ either `[movies|tv]`")
				api.PostMessage(event.Channel, slack.MsgOptionText(msg, true), params)
			} else if strings.Contains(sourceFile, "..") || strings.HasPrefix(sourceFile, "/") {
				msg := fmt.Sprintf("Please specify file to move relative to `%s/torrents/`\n", mediaPath)
				api.PostMessage(event.Channel, slack.MsgOptionText(msg, true), params)
			} else {
				MoveTorrentFile(sourceFile, destinationDir)
			}
		} else {
			msg := "Please provide a src file and destination [e.g. `movies` or `tv`]"
			api.PostMessage(event.Channel, slack.MsgOptionText(msg, true), params)
		}
	} else if args[0] == "porq" {
		var response string
		if len(args) > 1 {
			searchString := strings.Join(args[1:], " ")
			searchStringURL := "/q.php?q=" + url.QueryEscape(searchString)

			response = parseTorrents(searchProxy(searchStringURL))
		} else {
			response = parseTop100(searchProxy("/precompiled/data_top100_207.json"))
		}

		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "morq" {
		var response = parseMovieTorrents(args[1:])
		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "torq" {
		var response = parseTVTorrents(args[1:])
		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "vpns" {
		api.PostMessage(event.Channel, slack.MsgOptionText(VpnPiTunnelChecks(), false), params)
	} else if args[0] == "vpnc" {
		response := "Please provide a new VPN server (hint: output from `vpns`)"
		if len(args) > 1 {
			vpnServerDomain := strings.ToUpper(scrubParamOfHTTPMagicCrap(args[1]))
			vpnServerDomain = strings.ReplaceAll(vpnServerDomain, "-", "_")

			// ensure vpnServerDomain has format e.g. NL_15
			var rxPat = regexp.MustCompile(`^[A-Za-z]{2}_[0-9]{2}`)
			if !rxPat.MatchString(vpnServerDomain) {
				response = "Provide a valid formatted VPN server (hint: output from `vpns`)"

			} else {
				response = updateVpnPiTunnel(vpnServerDomain)
			}
		}
		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "version" {
		fingerprint := getDeployFingerprint("/root/.ssh/id_ed25519-cert.pub")
		response := ":gitlab: <https://gitlab.com/ackersonde/bender-slackbot/-/pipelines/" +
			gitlabRunID + "|" + gitlabRunID + "> using :key: " + fingerprint
		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "aw" {
		response := ":partly_sunny_rain: <https://openweathermap.org/city/2954088|8d forecast Au>"
		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "vil" || args[0] == "vis" || args[0] == "vir" {
		response := ""
		if args[0] == "vil" {
			response = fetchVaultImages()
		} else if args[0] == "vis" && len(args) == 2 && event.User == "U092UC9EW" {
			response = storeVaultImage(event, args[1])
		} else if args[0] == "vir" && len(args) == 2 && event.User == "U092UC9EW" {
			response = retrieveVaultImage(api, event, args[1])
		} else {
			response = "Usage: vi[l|s|r] <image name>"
		}
		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "trans" || args[0] == "trand" || args[0] == "tranc" || args[0] == "tranp" {
		response := torrentCommand(args)
		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "mvv" {
		response := "<" + mvvRoute("Schwabhausen", "München, Hauptbahnhof") + "|Going in>"
		response += " | <" + mvvRoute("München, Hauptbahnhof", "Schwabhausen") + "|Going home>"

		response += "\n" + fetchAktuelles()

		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "www" {
		betterStack := ":betterstack: <https://status.ackerson.de/|uptime> | <https://uptime.betterstack.com/team/216305/monitors|monitors>\n"

		hetzner := ":htz_server: <https://vault.ackerson.de/ui/|vault> | <https://sync.ackerson.de|syncthing> | <https://ackerson.de|homepage>\n"

		chore := ":raspberry_pi: <https://homesync.ackerson.de|syncthing> | <https://photos.ackerson.de/|photoprism> | <https://backvault.ackerson.de/ui/|test vault>\n"
		tuxedo := ":protonvpn: <https://vpnission.ackerson.de/transmission/web/|transmission> | <https://jelly.ackerson.de:8443/web/index.html#!/home.html|jelly>\n"

		fritzBox := ":house: <https://fritz.box/|fritzbox> | <https://freedns.afraid.org/dynamic/v2/|afraid>\n"

		response := betterStack + hetzner + chore + tuxedo + fritzBox
		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "key" {
		response := getBendersCurrentSSHCert()
		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "security" {
		response := checkFirewallRules(true)
		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if args[0] == "photos" || args[0] == "fotos" {
		response := "Available public photo albums on :photoprism::\n=========================\n"
		albums := buildPhotoPrismAlbums()
		if albums == "" {
			response = "No public photo albums available on :photoprism:"
		} else {
			response += albums
		}
		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	} else if inputIsChatGPTthread(event) || strings.HasPrefix(args[0], "hey") || strings.HasPrefix(args[0], "how") { // don't trigger OpenAI requests to any thread
		promptInput := strings.Join(args[1:], " ")

		promptAndReactor := strings.Split(promptInput, "[[")
		chatSystemRole := defaultSystemRole
		if len(promptAndReactor) > 1 {
			chatSystemRole = strings.TrimSuffix(promptAndReactor[1], "]]")
		}
		client, req := initGPTRequest(event, chatSystemRole)

		if args[0] == "how" { // prepend "how" onto prompt
			promptAndReactor[0] = "how " + promptAndReactor[0]
		}
		api.PostMessage(event.Channel, slack.MsgOptionTS(event.TimeStamp),
			slack.MsgOptionText(chat(client, req, promptAndReactor[0]), false), params)
	} else if args[0] == "dalle" {
		response := ""
		promptIndex := 1

		style := openai.CreateImageStyleNatural
		if strings.HasPrefix(args[1], "[") {
			if args[1] == "[vivid]" {
				style = openai.CreateImageStyleVivid
			}
			promptIndex += 1
		}

		size := openai.CreateImageSize1792x1024 // landscape - portrait is 1024x1792
		if strings.HasPrefix(args[promptIndex+1], "[") {
			if args[promptIndex+1] == "[portrait]" {
				size = openai.CreateImageSize1024x1792
			}
			promptIndex += 1
		}

		images := dalleImage(strings.Join(args[promptIndex:], " "), style, size)
		for i, image := range images {
			response += "<" + image.URL + "|Image " + strconv.Itoa(i+1) + ">\n"
		}
		chatCost := strconv.FormatFloat(imageCostUSDdallE, 'f', 3, 64)
		response += "\n\n" + openAIChatDelimeter + "\nA " + style + " HD, " + size + " image using " + usedDallEModel + " image = $" + chatCost + "\n"

		api.PostMessage(event.Channel, slack.MsgOptionTS(event.TimeStamp),
			slack.MsgOptionText(response, false), params)
	} else if args[0] == "aimage" {
		response := ""
		if len(event.Files) <= 0 {
			response = "Please try again with an uploaded image"
		} else {
			if len(args) == 1 {
				// provide default prompt
				args = append(args, "What's going on in this picture?")
			}
			filepath := os.TempDir() + string(os.PathSeparator) + event.ClientMsgID + "-" + event.Files[0].Name
			file, err := os.Create(filepath)

			if err != nil {
				response = fmt.Sprintf("Error opening file: %s (%s)\n", filepath, err)
			} else {
				err = api.GetFile(event.Files[0].URLPrivateDownload, file)
				Logger.Printf("downloading %s to %s\n", event.Files[0].URLPrivateDownload, filepath)
				if err != nil {
					Logger.Println("error downloading file: ", err)
				}
				defer file.Close()

				bytes, err := os.ReadFile(filepath)
				if err != nil {
					response = fmt.Sprintf("Error reading file: %s (%s)\n", filepath, err)
				} else {
					mimeType := http.DetectContentType(bytes)
					base64String := base64.StdEncoding.EncodeToString(bytes)
					base64Image := "data:" + mimeType + ";base64," + base64String

					imageConfig, _, err := image.DecodeConfig(strings.NewReader(string(bytes)))
					if err != nil {
						Logger.Printf("Error decoding image: %s (%s)\n", filepath, err)
					}

					response = interpretImage(imageConfig, base64Image, strings.Join(args[1:], " "))
				}
			}

			api.PostMessage(event.Channel, slack.MsgOptionTS(event.TimeStamp),
				slack.MsgOptionText(response, false), params)

			// cleanup after ourselves
			err = os.Remove(filepath)
			if err != nil {
				Logger.Println("error removing file: ", err)
			}
		}
	} else if args[0] == "translate" || args[0] == "transcribe" {
		response := ""

		if len(event.Files) <= 0 {
			response = "Please try again with a recorded voice message or uploaded file"
		} else {
			filepath := os.TempDir() + string(os.PathSeparator) + event.ClientMsgID + "-" + event.Files[0].Name
			file, err := os.Create(filepath)
			if err != nil {
				response = fmt.Sprintf("Error opening file: %s (%s)\n", filepath, err)
			} else {
				defer file.Close()
				err = api.GetFile(event.Files[0].URLPrivateDownload, file)
				Logger.Printf("downloading %s to %s\n", event.Files[0].URLPrivateDownload, filepath)

				if err != nil {
					Logger.Println("error downloading file: ", err)
				}
				response = whisperAudio(event, filepath, args[0])
				//response = fmt.Sprintf("%#v\n", event.Files[0].URLPrivateDownload)
			}

			// cleanup after ourselves
			err = os.Remove(filepath)
			if err != nil {
				Logger.Println("error removing file: ", err)
			}
		}

		// duration is NOT coming back from Whisper requests
		//chatCost := strconv.FormatFloat(duration*translationCostUSDperMinute, 'f', 3, 64)
		//durationStr := strconv.FormatFloat(duration*translationCostUSDperMinute, 'f', 2, 64)
		// response+"\n\n"+openAIChatDelimeter+"\n"+durationStr+" min(s) w/ Whisper-1 = $"+chatCost+"\n"
		api.PostMessage(event.Channel, slack.MsgOptionTS(event.TimeStamp),
			slack.MsgOptionText(response, false), params)

	} else if args[0] == "help" {
		response :=
			":ethereum: `crypto`: Current cryptocurrency stats :lumens:\n" +
				":sleuth_or_spy: `pgp`: PGP keys\n" +
				":ninja: `h4X07s`: Show remote IPs accessing sites\n" +
				":vault: `vfa <get (keyname)| put keyname secret>`: Vault Factor Auth (TOTP)\n" +
				":camera_with_flash: `vi[l|s|r]`: Vault Image [L]ist, [S]tore, [R]etrieve sensitive images\n" +
				":fritzbox: `vcu`: Update Vault Approle CIDRs to home & docker local IPs for access permission\n" +
				":key: `pass <64 10 10 false true>`: random pass with <chars digits symbols noUpper repeatChars>\n" +
				":sun_behind_rain_cloud: `aw`: Au weather\n" +
				":baseball: `bb <YYYY-MM-DD>`: show baseball games from given date (default yesterday)\n" +
				":htz_server: `htz|htzd <id>`: show|delete Hetzner server(s)\n" +
				":wifi: `wf [0|1|s]`: turn home wifi [0]ff, [1]n or [-default-s]tatus\n" +
				":protonvpn: `vpn[s|c]`: [S]how status of VPN on :raspberry_pi:, [C]hange VPN to `/etc/wireguard/<file>.conf` name\n" +
				":pirate_bay: `porq|morq|torq <Pirate Bay|YTS|eztv search-term>`\n" +
				":transmission: `tran[c|p|s|d]`: [C]reate <URL>, [P]aused <URL>, [S]tatus, [D]elete <ID> torrents on :raspberry_pi:\n" +
				":movie_camera: `mv " + mediaPath + "/torrents/<filename> [movies|tv/(<path>)]`\n" +
				":youtube: `yt <video url>`: Download Youtube video to Papa's handy\n" +
				":floppy_disk: `fsck`: show storage space and issues on remote servers\n" +
				":bar_chart: `pi`: Stats of various :raspberry_pi:s\n" +
				":gitlab: `version`: Which build/deploy is this Bender bot?\n" +
				":earth_americas: `www`: Show various internal links\n" +
				":photoprism: `photos`: Available public photo albums and stats\n" +
				":closed_lock_with_key: `security`: overview of SSH key(s) and UFW rules\n" +
				":whale2: `logs <container>`: last 100 lines of docker logs from <container> on ackerson.de\n" +
				":openai: `hey|how <prompt ([[you are a helpful chatbot]])>` e.g. 'How do I cook spaghetti? [[you are Massimo Bottura]]': chat with OpenAI's " + usedOpenAIModel + "\n" +
				":frame_with_picture: `dalle ([natural|vivid] [landscape|portrait]) <prompt>` asks OpenAI's " + usedDallEModel + " to generate an image from the prompt (takes ~30secs so be patient!)\n" +
				":microphone: `translate|transcribe <recorded/uploaded file>`: translate|transcribe audio via OpenAI's Whisper\n" +
				":thinking_face: `aimage <(What's going on in this picture?)>`: Ask OpenAI to interpret an uploaded image (png|jpg)\n"
		api.PostMessage(event.Channel, slack.MsgOptionText(response, true), params)
	} else if event.ThreadTimeStamp == "" { // only respond to direct requests
		response := "Whaddya say <@" + user.Profile.DisplayName + ">? Try `help` instead"
		api.PostMessage(event.Channel, slack.MsgOptionText(response, false), params)
	}
}

func getBendersCurrentSSHCert() string {
	response := ""
	out, err := exec.Command("ssh-keygen", "-L", "-f", "/root/.ssh/id_ed25519-cert.pub").Output()
	if err != nil {
		response += err.Error()
	} else {
		scanner := bufio.NewScanner(strings.NewReader(string(out)))
		for scanner.Scan() {
			text := strings.Trim(scanner.Text(), " ")
			if strings.HasPrefix(text, "Serial:") {
				response = text
				continue
			} else if strings.HasPrefix(text, "Valid:") {
				valid := text
				// Valid: from 2021-02-02T13:44:00 to 2021-03-09T13:45:01
				re := regexp.MustCompile(`Valid: from (?P<start>.*) to (?P<expire>.*)`)
				matches := re.FindAllStringSubmatch(text, -1)
				names := re.SubexpNames()

				m := map[string]string{}
				if len(matches) > 0 {
					for i, n := range matches[0] {
						m[names[i]] = n
					}
					if len(m) > 1 {
						expiry, err := time.Parse("2006-01-02T15:04:05", m["expire"])
						if err != nil {
							Logger.Printf("Unable to parse expiry date: %s", m["expire"])
						} else {
							today := time.Now()
							if expiry.Before(today) {
								valid += "\n" + ":rotating_light: Cert is expired! Please check `/var/log/gen_new_deploy_keys.log` on chore." +
									"\nOr just <https://gitlab.com/ackersonde/bender-slackbot/-/pipelines|redeploy bender> ..."
							} else {
								daysValid := expiry.Sub(today).Hours() / 24
								valid += "\nSSH Certificate valid for " + strconv.FormatFloat(daysValid, 'f', 0, 64) + " days"
							}
						}
					} else {
						Logger.Printf("Unable to parse validity: %s", valid)
					}
				} else {
					Logger.Printf("ERR: PUB CERT invalid date: %s", valid)
				}

				response += "\n" + valid
				break
			}
		}
	}

	return response
}

func fetchAktuelles() string {
	rndString := strconv.FormatInt(time.Now().UnixNano(), 10)
	url := "https://db-streckenagent.hafas.de/newsletter/gate?rnd=" + rndString

	// Goto https://www.s-bahn-muenchen.de/s_muenchen/view/service/aktuelle_betriebslage.shtml w/ DevTools enabled
	// inspect REQs like https://db-streckenagent.hafas.de/newsletter/gate?rnd=xyz
	// as I expect the post data below may change regularly :(
	var postData = []byte(`{"id":"ssww7rjiiqci9m88","ver":"1.25","lang":"deu","auth":{"type":"AID","aid":"da39a3ee5e6b4"},"client":{"id":"HAFAS","type":"WEB","name":"webapp","l":"vs_webapp"},"formatted":false,"svcReqL":[{"req":{"getChildren":true,"getParent":true,"maxNum":500,"himFltrL":[{"mode":"INC","type":"CH","value":"CUSTOM1"}],"sortL":["LMOD_DESC"]},"meth":"HimSearch","id":"1|1|"}]}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(postData))
	if err != nil {
		Logger.Printf("ERR prep request: %s", err.Error())
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		Logger.Printf("ERR make request: %s", err.Error())
	}
	defer resp.Body.Close()

	response, err := io.ReadAll(resp.Body)
	if err != nil {
		Logger.Printf("ERR read resp: %s", err.Error())
	}

	/* TEST response
	response := []byte(`
	{"ver":"1.25","lang":"deu","id":"ssww7rjiiqci9m88","err":"OK","graph":{"id":"standard","index":0},"subGraph":{"id":"global","index":0},"view":{"id":"standard","index":0,"type":"WGS84"},"svcResL":[{"id":"1|1|","meth":"HimSearch","err":"OK","res":{"common":{"locL":[{"lid":"A=1@O=München Ost@X=11604975@Y=48127437@U=80@L=8000262@","type":"S","name":"München Ost","icoX":0,"extId":"8000262","state":"F","crd":{"x":11604993,"y":48127302,"z":0},"pCls":447},{"lid":"A=1@O=München Donnersbergerbrücke@X=11536540@Y=48142620@U=80@L=8004128@","type":"S","name":"München Donnersbergerbrücke","icoX":1,"extId":"8004128","state":"F","crd":{"x":11537133,"y":48142683,"z":0},"pCls":56}],"prodL":[{"name":"S 1"},{"name":"S 6"},{"name":"S 7"},{"name":"S 8"},{"name":"S 2"}],"icoL":[{"res":"prod_ice","fg":{"r":255,"g":255,"b":255},"bg":{"r":40,"g":45,"b":55}},{"res":"prod_reg","fg":{"r":255,"g":255,"b":255},"bg":{"r":175,"g":180,"b":187}},{"res":"HIM1"}],"himMsgEdgeL":[{"icoCrd":{"x":11570757,"y":48135028}}],"himMsgCatL":[{"id":1}],"gTagL":["titleText","emailTitle","operationalSituationTitle","operationalSituation","email"]},"msgL":[{"hid":"RIS_HIM_FREETEXT_1080490","act":true,"head":"Bauarbeiten.","icoX":2,"prio":50,"fLocX":0,"tLocX":1,"prod":65535,"affProdRefL":[0,1,2,3,4],"src":99,"lModDate":"20200529","lModTime":"202744","sDate":"20200529","sTime":"223000","eDate":"20200601","eTime":"043000","sDaily":"000000","eDaily":"235900","comp":"Region Bayern","catRefL":[0],"pubChL":[{"name":"EMAIL","fDate":"20200529","fTime":"201500","tDate":"20200601","tTime":"043000"},{"name":"CUSTOM1","fDate":"20200529","fTime":"201500","tDate":"20200601","tTime":"043000"}],"edgeRefL":[0],"texts":[{"gTagXL":[0],"texts":[{"text":"Bauarbeiten."}]},{"gTagXL":[1,2],"texts":[{"text":"Stammstrecke: Bauarbeiten von Freitag, 29. Mai, 22.30 Uhr bis Montag, 01. Juni 2020, 4.30 Uhr zwischen München Ost und München-Pasing"}]},{"gTagXL":[3,4],"texts":[{"text":"Wegen Bauarbeiten zur 2.Stammstrecke kommt es von Freitag, 29. Mai (22:30 Uhr) durchgehend bis Montag, 1. Juni 2020 (4:30 Uhr) zwischen München Ost und München-Pasing zu Fahrplanänderungen mit Umleitungen und Haltausfällen auf fast allen S-Bahn-Linien. <br><br>Zwischen München Ost und München-Pasing verkehren nur die Linien S 6 und S 7 regulär durch die Stammstrecke.<br>Zwischen München-Ost und Hackerbrücke besteht am Samstag, jeweils von 9 bis 1 Uhr und am Sonntag, jeweils von 11 bis 21 Uhr ein Pendelverkehr im 20-Minuten-Takt.<br><br>Weitere Informationen, sowie die Fahrpläne der einzelnen Linien finden Sie unter https://t1p.de/94jj"}]}]}]}}]}`)
	*/

	svcResL := jsoniter.Get(response, "svcResL", 0).ToString()
	res := jsoniter.Get([]byte(svcResL), "res").ToString()
	common := jsoniter.Get([]byte(res), "common").ToString()
	if common == "{}" {
		return "Aktuell liegen uns keine Meldungen vor."
	}

	aktuell := ""
	lines := strings.ToLower(jsoniter.Get([]byte(common), "prodL", '*').ToString())
	if lines != "" {
		effectedTrains := []map[string]string{}
		effectedTrainLogos := ""

		err2 := json.Unmarshal([]byte(lines), &effectedTrains)
		if err2 != nil {
			Logger.Printf("ERR parsing trains: %s", err2.Error())
		} else {
			for _, train := range effectedTrains {
				logo := strings.Replace(train["name"], " ", "", 1)
				effectedTrainLogos += fmt.Sprintf(":mvv_" + logo + ": ")
			}
			effectedTrainLogos += "\n"
			aktuell = fmt.Sprintf("%s\n", effectedTrainLogos)
		}
	}

	msgL := jsoniter.Get([]byte(res), "msgL", 0).ToString()
	titleTexts := jsoniter.Get([]byte(msgL), "texts", 1).ToString()
	gTagXL1 := jsoniter.Get([]byte(titleTexts), "texts", 0).ToString()
	titleText := jsoniter.Get([]byte(gTagXL1), "text").ToString()

	subjectTexts := jsoniter.Get([]byte(msgL), "texts", 2).ToString()
	gTagXL2 := jsoniter.Get([]byte(subjectTexts), "texts", 0).ToString()
	subjectText := jsoniter.Get([]byte(gTagXL2), "text").ToString()
	subjectText = strings.ReplaceAll(subjectText, "<br>", "\n")

	aktuell += fmt.Sprintf("%s\n\n%s\n\n", titleText, subjectText)

	lModDate := jsoniter.Get([]byte(msgL), "lModDate").ToString()
	lModTime := jsoniter.Get([]byte(msgL), "lModTime").ToString()
	if lModDate != "" && lModTime != "" {
		lastUpdate, _ := time.Parse("20060102150405", lModDate+lModTime)
		aktuell += fmt.Sprintf("_update von %s_", lastUpdate.Format("02-Jan-2006 15:04"))
	}

	return aktuell
}

func mvvRoute(origin string, destination string) string {
	loc, _ := time.LoadLocation("Europe/Berlin")
	date := time.Now().In(loc)

	yearObj := date.Year()
	monthObj := int(date.Month())
	dayObj := date.Day()
	hourObj := date.Hour()
	minuteObj := date.Minute()

	month := strconv.Itoa(monthObj)
	hour := strconv.Itoa(hourObj)
	day := strconv.Itoa(dayObj)
	minute := strconv.Itoa(minuteObj)
	year := strconv.Itoa(yearObj)

	return "http://efa.mvv-muenchen.de/mvv/XSLT_TRIP_REQUEST2?&language=de" +
		"&anyObjFilter_origin=0&sessionID=0&itdTripDateTimeDepArr=dep&type_destination=any" +
		"&itdDateMonth=" + month + "&itdTimeHour=" + hour + "&anySigWhenPerfectNoOtherMatches=1" +
		"&locationServerActive=1&name_origin=" + origin + "&itdDateDay=" + day + "&type_origin=any" +
		"&name_destination=" + destination + "&itdTimeMinute=" + minute + "&Session=0&stateless=1" +
		"&SpEncId=0&itdDateYear=" + year
}
