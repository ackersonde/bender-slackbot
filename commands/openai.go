package commands

import (
	"context"
	"errors"
	"fmt"
	"image"
	"io"
	"math"
	"os"
	"strconv"
	"strings"

	"github.com/sashabaranov/go-openai"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
)

var usedOpenAIModel = openai.GPT4o             // (128K)
var tokenCostUSDUsedOpenAIModel = 0.015 / 1000 // GPT4o w/ 128K context cost per 1K tokens

var openAIChatDelimeter = "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+"
var defaultSystemRole = "you are a helpful chatbot"

var usedDallEModel = openai.CreateImageModelDallE3 // (512)
var imageCostUSDdallE = 0.005                      // cost per 1 HD image (1024/1792px)

func interpretImage(imageConfig image.Config, base64Image, prompt string) string {
	req := openai.ChatCompletionRequest{
		MaxTokens: 4096,
		Model:     openai.GPT4o,
		Messages: []openai.ChatCompletionMessage{
			{
				Role: openai.ChatMessageRoleUser,
				MultiContent: []openai.ChatMessagePart{
					{
						Type: openai.ChatMessagePartTypeText,
						Text: prompt,
					},
					{
						Type: openai.ChatMessagePartTypeImageURL,
						ImageURL: &openai.ChatMessageImageURL{
							URL:    base64Image,
							Detail: openai.ImageURLDetailAuto,
						},
					},
				},
			},
		},
		Stream: true,
	}

	client := openai.NewClient(os.Getenv("OPENAI_API_KEY"))
	stream, err := client.CreateChatCompletionStream(context.Background(), req)
	if err != nil {
		fmt.Printf("ChatCompletionStream error: %v\n", err)
		return err.Error()
	}
	defer stream.Close()

	var content string
	for {
		var response openai.ChatCompletionStreamResponse
		response, err = stream.Recv()
		if errors.Is(err, io.EOF) {
			break
		} else if err != nil {
			return err.Error()
		}

		content += response.Choices[0].Delta.Content
	}

	imageTokens := calculateImageTokens(float64(imageConfig.Width), float64(imageConfig.Height))
	respTokens := float64(len(content)) / 4

	// https: //openai.com/pricing#language-models
	cost := imageTokens/1000*float64(0.01) + respTokens/1000*0.015
	chatCost := strconv.FormatFloat(cost, 'f', 3, 64)
	return content + "\n\n" + openAIChatDelimeter + "\n" + strconv.Itoa(int(imageTokens+respTokens+0.5)) +
		" <https://openai.com/pricing#language-models|tokens> w/ GPT4o = $" + chatCost
}

// https://platform.openai.com/docs/guides/vision/calculating-costs
func calculateImageTokens(imageWidth, imageHeight float64) (tokens float64) {
	var tiles float64
	var long float64

	short := math.Min(imageWidth, imageHeight)
	if short == imageWidth {
		long = imageHeight
	} else {
		long = imageWidth
	}

	// longest side <= 768, then we have no reduction and take base cost
	if long <= 512 {
		tiles = 1 // one tile minimum
	} else if long > 2048 { // then we must reduce the image to 2048x2048 box
		factor := 2048 / long
		return calculateImageTokens(imageWidth*factor, imageHeight*factor)
	}

	if short <= 768 { // then we have no reduction and calculate tiles directly
		tiles = calculateTiles(short, long)
	} else { // resize
		factor := 768 / short
		return calculateImageTokens(imageWidth*factor, imageHeight*factor)
	}

	return (tiles * 170) + 85 // where 170 is tokens/tile & 85 is base token cost
}

func calculateTiles(shortSide, longSide float64) float64 {
	var shortTiles, longTiles float64
	shortTiles = math.Ceil(shortSide / 512)
	longTiles = math.Ceil(longSide / 512)

	return shortTiles * longTiles
}

func whisperAudio(event *slackevents.MessageEvent, filepath string, action string) string {
	prompt := ""
	if action == "translate" {
		prompt = "English text if not English"
	}

	req := openai.AudioRequest{
		Model:    openai.Whisper1, // "whisper-1" => $0.006 / minute
		FilePath: filepath,
		Prompt:   prompt,
	}

	client := openai.NewClient(os.Getenv("OPENAI_API_KEY"))
	var resp openai.AudioResponse
	var err error
	if action == "translate" {
		resp, err = client.CreateTranslation(context.Background(), req)
		if err != nil {
			Logger.Printf("Translation creation error: %v\n", err)
		}
		// translate doesn't work, so I go back to OpenAI $$$
		client2, chatReq := initGPTRequest(event, defaultSystemRole)
		return chat(client2, chatReq, "translate to English: '"+resp.Text+"'")
	} else if action == "transcribe" {
		resp, err = client.CreateTranscription(context.Background(), req)
	}
	if err != nil {
		return fmt.Sprintf("Audio creation error: %v\n", err)
	}
	Logger.Printf("Audio response: %#v\n", resp)

	return resp.Text
}

func dalleImage(prompt string, style string, size string) []openai.ImageResponseDataInner {
	var response []openai.ImageResponseDataInner

	client := openai.NewClient(os.Getenv("OPENAI_API_KEY"))
	respUrl, err := client.CreateImage(
		context.Background(),
		openai.ImageRequest{
			Model:          usedDallEModel,
			Prompt:         prompt,
			Quality:        openai.CreateImageQualityHD,
			Size:           size,
			Style:          style,
			ResponseFormat: openai.CreateImageResponseFormatURL,
			N:              1,
		},
	)
	if err != nil {
		Logger.Printf("Image creation error: %v\n", err)
		return response
	}

	return respUrl.Data
}

func inputIsChatGPTthread(event *slackevents.MessageEvent) bool {
	result := false

	if event.ThreadTimeStamp != "" { // if this is reply to a thread
		parentMsg, err := api.GetConversationHistory(&slack.GetConversationHistoryParameters{
			ChannelID: event.Channel, Limit: 1, Oldest: event.ThreadTimeStamp, Inclusive: true})

		if err != nil {
			Logger.Printf("Error getting conversation history: %s", err)
		}
		if strings.HasPrefix(parentMsg.Messages[0].Text, "hey") || strings.HasPrefix(parentMsg.Messages[0].Text, "how") {
			return true // and parent msg starts with "hey" or "how", we are in a ChatGPT thread
		}
	}

	return result
}

// Init client and request (incl. entire Thread conversation for context) to OpenAI API
func initGPTRequest(event *slackevents.MessageEvent, chatCompletionReactor string) (*openai.Client, openai.ChatCompletionRequest) {
	client := openai.NewClient(os.Getenv("OPENAI_API_KEY"))
	req := openai.ChatCompletionRequest{
		Model: usedOpenAIModel,
		Messages: []openai.ChatCompletionMessage{
			{
				Role:    openai.ChatMessageRoleSystem,
				Content: chatCompletionReactor,
			},
		},
	}

	hasMore := true
	nextCursor := ""
	err := error(nil)
	var replies []slack.Message

	// dump thread replies into request for context
	for hasMore {
		repliesParams := slack.GetConversationRepliesParameters{
			ChannelID: event.Channel,
			Timestamp: event.ThreadTimeStamp,
			Cursor:    nextCursor,
		}
		replies, hasMore, nextCursor, err = api.GetConversationReplies(&repliesParams)
		if hasMore && err != nil {
			Logger.Printf("Error getting conversation replies: %s", err)
		}
		var msg openai.ChatCompletionMessage
		for _, reply := range replies {
			footer := strings.Split(reply.Text, openAIChatDelimeter)
			if reply.User == os.Getenv("SLACK_BENDER_BOT_USERID") {
				msg = openai.ChatCompletionMessage{
					Role:    openai.ChatMessageRoleAssistant,
					Content: strings.TrimSpace(footer[0]), // remove token counter
				}
				reactorAndTokens := strings.Split(footer[1], "]]")
				reactor := strings.TrimPrefix(strings.TrimSpace(reactorAndTokens[0]), "[[")
				req.Messages[0].Content = reactor // maintain thread's reactor
			} else {
				msg = openai.ChatCompletionMessage{
					Role:    openai.ChatMessageRoleUser,
					Content: reply.Text,
				}
			}
			req.Messages = append(req.Messages, msg)
		}
	}

	return client, req
}

func chat(client *openai.Client, req openai.ChatCompletionRequest, prompt string) string {
	req.Messages = append(req.Messages, openai.ChatCompletionMessage{
		Role:    openai.ChatMessageRoleUser,
		Content: prompt,
	})

	resp, err := client.CreateChatCompletion(context.Background(), req)
	if err != nil {
		return fmt.Sprintf("ChatCompletion error: %v\n", err)
	}
	//jsonBytes, err := json.Marshal(resp)
	//Logger.Println(string(jsonBytes), err)

	chatCost := strconv.FormatFloat(float64(resp.Usage.TotalTokens)*tokenCostUSDUsedOpenAIModel, 'f', 3, 64)
	msg := resp.Choices[0].Message.Content + "\n\n" + openAIChatDelimeter + "\n" +
		"[[" + req.Messages[0].Content + "]]\n" +
		strconv.Itoa(resp.Usage.TotalTokens) +
		" <https://openai.com/pricing#language-models|tokens> w/ " +
		usedOpenAIModel + " = $" + chatCost

	return msg
}
