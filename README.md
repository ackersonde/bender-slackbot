[![pipeline status](https://gitlab.com/ackersonde/bender-slackbot/badges/master/pipeline.svg)](https://gitlab.com/ackersonde/bender-slackbot/-/commits/master)

# bender-slackbot

Slackbot for ackerson.slack.com

[Overview in this blog post](https://agileweboperations.com/write-your-own-slack-chatbot-in-golang)

<img src="https://juststickers.in/wp-content/uploads/2016/07/go-programming-language.png" width="30%">![Plus](http://icons.iconarchive.com/icons/icons8/ios7/64/User-Interface-Plus-icon.png) ![Slack](https://a.slack-edge.com/0180/img/icons/app-256.png)&nbsp; &nbsp; ![Equals](https://icons.iconarchive.com/icons/fa-team/fontawesome/48/FontAwesome-Equals-icon.png) &nbsp; <img src="https://upload.wikimedia.org/wikipedia/en/a/a6/Bender_Rodriguez.png" height="20%" width="20%">
